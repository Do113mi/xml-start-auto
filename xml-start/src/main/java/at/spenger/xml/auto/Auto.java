package at.spenger.xml.auto;

import java.sql.Date;

import javax.xml.bind.annotation.XmlElement;

public class Auto {

	@XmlElement(required = true)
	protected String marke;
	@XmlElement(required = true)
	protected String modell;
	@XmlElement(required = true)
	protected String typ;
	@XmlElement(required = true)
	protected double preis;
	@XmlElement(required = true)
	protected double ps;
	@XmlElement(required = true)
	protected String farbe;
	@XmlElement(required = true)
	protected String sitze;
	@XmlElement(required = true)
	protected int reifen;
	@XmlElement(required = true)
	protected Date erstZulass;
	@XmlElement(required = true)
	protected String baujahr;

	public String getMarke() {
		return marke;
	}

	public void setMarke(String marke) {
		this.marke = marke;
	}

	public String getModell() {
		return modell;
	}

	public void setModell(String modell) {
		this.modell = modell;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public double getPs() {
		return ps;
	}

	public void setPs(double ps) {
		this.ps = ps;
	}

	public String getFarbe() {
		return farbe;
	}

	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}

	public String getSitze() {
		return sitze;
	}

	public void setSitze(String sitze) {
		this.sitze = sitze;
	}

	public int getReifen() {
		return reifen;
	}

	public void setReifen(int reifen) {
		this.reifen = reifen;
	}

	public Date getErstZulass() {
		return erstZulass;
	}

	public void setErstZulass(Date erstZulass) {
		this.erstZulass = erstZulass;
	}

	public String getBaujahr() {
		return baujahr;
	}

	public void setBaujahr(String string) {
		this.baujahr = string;
	}

	

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((baujahr == null) ? 0 : baujahr.hashCode());
		result = prime * result
				+ ((erstZulass == null) ? 0 : erstZulass.hashCode());
		result = prime * result + ((farbe == null) ? 0 : farbe.hashCode());
		result = prime * result + ((marke == null) ? 0 : marke.hashCode());
		result = prime * result + ((modell == null) ? 0 : modell.hashCode());
		long temp;
		temp = Double.doubleToLongBits(preis);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(ps);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + reifen;
		result = prime * result + ((sitze == null) ? 0 : sitze.hashCode());
		result = prime * result + ((typ == null) ? 0 : typ.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Auto other = (Auto) obj;
		if (baujahr == null) {
			if (other.baujahr != null)
				return false;
		} else if (!baujahr.equals(other.baujahr))
			return false;
		if (erstZulass == null) {
			if (other.erstZulass != null)
				return false;
		} else if (!erstZulass.equals(other.erstZulass))
			return false;
		if (farbe == null) {
			if (other.farbe != null)
				return false;
		} else if (!farbe.equals(other.farbe))
			return false;
		if (marke == null) {
			if (other.marke != null)
				return false;
		} else if (!marke.equals(other.marke))
			return false;
		if (modell == null) {
			if (other.modell != null)
				return false;
		} else if (!modell.equals(other.modell))
			return false;
		if (Double.doubleToLongBits(preis) != Double
				.doubleToLongBits(other.preis))
			return false;
		if (Double.doubleToLongBits(ps) != Double.doubleToLongBits(other.ps))
			return false;
		if (reifen != other.reifen)
			return false;
		if (sitze == null) {
			if (other.sitze != null)
				return false;
		} else if (!sitze.equals(other.sitze))
			return false;
		if (typ == null) {
			if (other.typ != null)
				return false;
		} else if (!typ.equals(other.typ))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Auto [marke=" + marke + ", modell=" + modell + ", typ=" + typ
				+ ", preis=" + preis + ", ps=" + ps + ", farbe=" + farbe
				+ ", sitze=" + sitze + ", reifen=" + reifen + ", erstZulass="
				+ erstZulass + ", baujahr=" + baujahr + "]";
	}

}
