package at.spenger.xml;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration 				// tells Spring to treat this as a configuration class and register it as a Bean
@ComponentScan 				// we define the package to load the classes from
@EnableAutoConfiguration 	// tells Spring to treat this class as a consumer of application.yml/properties values
public class DefaultConfig {
	

}
